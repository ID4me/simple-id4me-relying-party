/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Peter Höbel peter.hoebel@open-xchange.com
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/

package org.id4me;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.id4me.config.Id4meClaimsParameters;
import org.id4me.config.Id4meProperties;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * Simple ID4me logon service to demonstrate how to use the ID4me Relying Party
 * api.
 * 
 * @author Peter Hoebel
 * 
 */
@RestController
public class SimpleID4meRelyingPartyController {
	// if you need the userinfo data, set userinfoWanted=true.
	final boolean userinfoWanted = true;

	@RequestMapping(value = "/")
	public void index(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		resp.sendRedirect("/index.html");
	}

	/**
	 * End point for the ID4me login form. The form field "id4me" contains the Id4ME
	 * id to log on. Redirects the client to the Id4ME authorization uri. or returns
	 * an error if something failes.
	 * 
	 * @param req
	 *            the servlet request
	 * @param resp
	 *            the servlet response
	 * @throws IOException
	 *             if sendError() or sendRedirect() fails
	 */
	@RequestMapping(value = "/login")
	public void login(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try {
			String id4me = req.getParameter("id4me");
			if (id4me == null || "".equals(id4me.trim())) {
				req.setAttribute("error", "Parameter id4me missing, or wrong format! Expected: user.example.com");
				resp.sendError(401);
				return;
			}

			Id4meProperties props = new Id4meProperties()
					.setRegistrationDataPath("/home/phoebel/ox-data/simple/")
					.setRedirectURI("http://tismail.de/logon")
					.setRedirectURIs(new String[] { "http://tismail.de/logon" })
					.setClientName("Simple-ID4me-Client")
					.setDnsResolver("8.8.8.8")
					.setDnssecRequired(false)
					.setFallbackToScopes(true);

			Id4meClaimsParameters claims = new Id4meClaimsParameters()
					.addEntry(new Id4meClaimsParameters.Entry().setName("email").setEssential(false)
							.setReason("Needed to create the profile"))
					.addEntry(
							new Id4meClaimsParameters.Entry().setName("name").setReason("Displayname in the user data"))
					.addEntry(new Id4meClaimsParameters.Entry().setName("address"));

			Id4meLogon logon_handler = new Id4meLogon(props, claims, new String[]{});

			Id4meSessionData session_data = logon_handler.createSessionData(id4me, true);

			if (session_data != null) {
				HttpSession session = req.getSession();
				session.removeAttribute("logon_data");
				session.setAttribute("logon_data", session_data);
				session.setAttribute("logon_handler", logon_handler);
				String authorizationUri = logon_handler.authorize(session_data);
				resp.sendRedirect(authorizationUri);
			}
		} catch (Exception ex) {
			resp.getWriter().println("Something went wrong: " + ex.getMessage());
			resp.setStatus(500);
		}
		return;
	}

	/**
	 * End point for the Id4ME authorization redirect uri. If an error occured the
	 * parameter "error" is set, otherwise the request must contain a parameter
	 * "code".
	 * 
	 * @param req
	 *            the servlet request
	 * @param resp
	 *            the servlet response
	 * @throws IOException
	 *             if sendError() or sendRedirect() fails
	 */
	@RequestMapping(value = "/logon")
	public void logon(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String error = req.getParameter("error");
		if (error != null) {
			String descr = req.getParameter("error_description");
			req.getSession().invalidate();
			resp.getWriter().println("Something went wrong: " + descr);
			resp.setStatus(500);
			return;
		}

		String code = req.getParameter("code");
		if (code == null || "".equals(code)) {
			resp.getWriter().println("Parameter code is missing in request!");
			resp.setStatus(500);
			return;
		}

		Id4meSessionData sessionData = (Id4meSessionData) req.getSession(false).getAttribute("logon_data");
		Id4meLogon id4meLogon = (Id4meLogon) req.getSession(false).getAttribute("logon_handler");

		try {
			id4meLogon.authenticate(sessionData, code);
			if (userinfoWanted) {
				id4meLogon.userinfo(sessionData);
			}

			resp.sendRedirect("/result");
		} catch (Exception ex) {
			resp.getWriter().println("Something went wrong: " + ex.getMessage());
			resp.setStatus(500);
		}
		return;
	}

	/**
	 * End point for the Id4ME authorization redirect uri. If an error occured the
	 * parameter "error" is set, otherwise the request must contain a parameter
	 * "code".
	 * 
	 * @param req
	 *            the servlet request
	 * @param resp
	 *            the servlet response
	 * @throws IOException
	 *             if sendError() or sendRedirect() fails
	 */
	@RequestMapping(value = "/result")
	public void result(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		Id4meSessionData sessionData = (Id4meSessionData) req.getSession(false).getAttribute("logon_data");
		String returnValue = "id4me: " + sessionData.getIdentityHandle();
		try {
			long expires = sessionData.getTokenExpires();
			if (expires != 0 && expires < System.currentTimeMillis()) {
				resp.getWriter().println("ID4me token expired");
				resp.setStatus(500);
				return;
			} else {
				if (expires != 0) {
					double e = (expires - System.currentTimeMillis());
					System.out.println("token expires in: " + Math.round(e * 0.001) + " sec.");
				}
			}
			if (userinfoWanted) {
				JSONObject userinfo = sessionData.getUserinfo();
				returnValue += "\n" + userinfo.toString(1);
			}
			resp.getWriter().println(returnValue);
		} catch (Exception ex) {
			resp.getWriter().println("Something went wrong: " + ex.getMessage());
			resp.setStatus(500);
		}
		return;
	}
}
